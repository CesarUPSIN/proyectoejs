const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");
const res = require("express/lib/response");
const json = require("body-parser");
const morgan = require("morgan");

/* 22/02/2023 */
const misRutas = require("./router/index.js");
const path = require("path");

const app = express();
app.use(express.json())

const cors = require('cors');
app.use(cors());

app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({extended:true}));

/* 22/02/2023 */
app.engine("html", require("ejs").renderFile); // Cambiar extensiones ejs a html
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(misRutas);

/* La página del error va al final de get/post */
app.use((req, res, next)=> {
    res.status(404).sendFile(__dirname + '/public/error.html');
});

const puerto = 3000;

app.listen(puerto, ()=> {
    console.log("Iniciando puerto");
});