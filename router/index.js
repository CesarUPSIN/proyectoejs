const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const AlumnosDb = require("../models/alumnos.js");
const axios = require('axios');

let bandera = 0;
let rows;


let datos = [ {
    matricula: "2019030399",
    nombre: "ACOSTA ORTEGA JESUS HUMBERTO",
    sexo: 'M',
    materias: ["Ingles", "Base de datos", "Tecnologías I"]
},
{
    matricula: "2020003007",
    nombre: "ACOSTA VARELA IRVING GUADALUPE",
    sexo: 'M',
    materias: ["Ingles", "Base de datos", "Tecnologías I"]
},
{
    matricula: "2017030714",
    nombre: "CESAR OSWALDO BERNAL SANCHEZ",
    sexo: 'M',
    materias: ["Ingles", "Base de datos", "Tecnologías I"]
}
];

router.get("/", (req, res)=> {
res.render("index.html", {titulo: "Mi primer página en Embedded Javascript", 
                        nombre: "César Oswaldo Bernal Sánchez", 
                        grupo: "TCI8-3", 
                        listado: datos});
});

/* Tabla */
router.get("/tabla", (req, res)=> {
    const params = {
        numero: req.query.numero
    }
    res.render("tabla.html", params);
});

router.post("/tabla", (req, res)=> {
    const params = {
        numero: req.body.numero
    }
    res.render("tabla.html", params);
});

/*Cotización*/
router.get("/cotizacion", (req, res)=> {
    const params = {
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazo: req.query.plazo,
    }
    res.render("cotizacion.html", params);
});

router.post("/cotizacion", (req, res)=> {
    const params = {
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazo: req.body.plazo,
    }
    res.render("cotizacion.html", params);
});

// Mostrar tabla de alumnos
router.get("/alumnos", async(req, res)=> {
    if(bandera == 0) {
        rows = await AlumnosDb.mostrarTodos();
    }
    else if(bandera == 1) {
        rows = await AlumnosDb.buscarMatricula(matricula);
        bandera = 0;
    }

    res.render("alumnos.html", {
        hola: rows
    });
});

//Insertar alumnos
router.post("/insertar", async(req, res)=> {
    try {
        let { matricula, nombre, domicilio, sexo, especialidad } = req.body;

        const nuevo = {
            matricula: matricula.trim(),
            nombre: nombre.trim(),
            domicilio: domicilio.trim(),
            sexo: sexo.trim(),
            especialidad: especialidad.trim()
        }

        const rows = await AlumnosDb.insertar(nuevo);

        res.status(200).send("Se insertaron con exito los datos");
    }
    catch (error) {
        console.error(error);
        res.status(400).send("Sucedio un error");
    }
});

router.post("/alumnos", async(req, res)=> {
    matricula = req.body.matricula;
    bandera = 1;
    rows = await AlumnosDb.buscarMatricula(matricula);

    res.render("alumnos.html", {hola: rows});
});

router.post('/borrar', async(req, res) => {
    try {
      const matricula = req.body.matricula;
      console.log(`Matrícula recibida: ${matricula}`);
      await AlumnosDb.borrar(matricula);
      res.render("alumnos.html", {hola: rows});
    } catch (error) {
      console.error(error);
      res.status(500).send('Sucedio un error al eliminar el estudiante');
    }
});

router.post('/modificar', async(req, res) => {
    try {
        let { matricula, nombre, domicilio, sexo, especialidad } = req.body;

        const nuevo = {
            matricula: matricula.trim(),
            nombre: nombre.trim(),
            domicilio: domicilio.trim(),
            sexo: sexo.trim(),
            especialidad: especialidad.trim()
        }

        rows = await AlumnosDb.actualizar(nuevo, nuevo.matricula);
        res.render("alumnos.html", {hola: rows});
    }
    catch (error) {
        console.error(error);
        res.status(400).send("Sucedio un error");
    }
});
  
module.exports = router;