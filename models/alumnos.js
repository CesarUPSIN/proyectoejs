const json = require("express/lib/response");
const resolve = require("path/posix");
const promise = require("../models/conexion.js");
const conexion = require("../models/conexion.js");

var AlumnosDb = {
    
};

AlumnosDb.insertar = function insertar(alumno) {
    return new Promise((resolve, reject)=> {
        var sqlConsulta = "insert into alumno set ? ";
        conexion.query(sqlConsulta, alumno, function(err, res) {
            if(err) {
                console.log("Surgio un error " + err.message);
                reject(err);
            }
            else {
                resolve({
                    id:res.id,
                    matricula:alumno.matricula,
                    nombre:alumno.nombre,
                    domicilio:alumno.domicilio,
                    sexo:alumno.sexo,
                    especialidad:alumno.especialidad
                });
            }
        });
    });
}

AlumnosDb.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject)=> {
        var sqlConsulta = "select *from alumno";
        conexion.query(sqlConsulta, null, function(err, res) {
            if(err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

AlumnosDb.buscarMatricula = function buscarMatricula(matricula) {
    return new Promise((resolve, reject)=> {
        var sqlConsulta = "select *from alumno where matricula = ?";
        conexion.query(sqlConsulta, [matricula], function(err, res) {
            if(err) {
                console.log("Surgio un error " + err.message);
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

AlumnosDb.borrar = function borrar(matricula) {
    return new Promise((res, reject)=> {
        var sqlConsulta = "delete from alumno where matricula = ?";
        conexion.query(sqlConsulta, [matricula], function(err, result) {
            if(err) {
                console.log("Surgio un error " + err.message);
                reject(err);
            }
            else {
                res(result.affectedRows);
            }
        });
    });
}

AlumnosDb.actualizar = function actualizar(nuevosDatos, matricula) {
    return new Promise((resolve, reject)=> {
        var sqlConsulta = "update alumno set ? where matricula = ?";
        conexion.query(sqlConsulta, [nuevosDatos, matricula], function(err, res) {
            if(err) {
                console.log("Surgio un error " + err.message);
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

module.exports = AlumnosDb;