"use strict";

const form = document.querySelector("#form");
const btnAgregar = document.querySelector("#btnAgregar");
const btnBuscar = document.querySelector("#btnBuscar");
const btnBorrar = document.querySelector("#btnBorrar");
const btnModificar = document.querySelector("#btnModificar");

const datos = () => {
    return {
        matricula: form['matricula'].value.trim().substring(0,45),
        nombre: form['nombre'].value.trim().substring(0,45),
        domicilio: form['domicilio'].value.trim().substring(0,45),
        sexo: form['sexo'].value.trim().substring(0,1),
        especialidad: form['especialidad'].value.trim().substring(0,45)
    };
};

async function insertar(event) {
    event.preventDefault();

    try {
        let { matricula, nombre, domicilio, sexo, especialidad } = datos();

        if(!matricula || !nombre || !domicilio || !sexo || !especialidad) {
            alert("Existen campos vacios");
        }
        else {
            await axios.post('/insertar', {
                matricula,
                nombre,
                domicilio,
                sexo,
                especialidad,
            }, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });

            setTimeout(() => {
                window.location.href = '/alumnos';
            }, 2000);
        }
    }
    catch (error) {
        console.log(error);
    }
}

async function buscar(event) {
    try {
		event.preventDefault();

		let matriculaCampo = datos();
        let matricula = matriculaCampo.matricula;
        console.log(matricula);
        
        if(!matricula) {
            alert("Agregar una matricula");
        }
        else {

            await axios.post('/alumnos', { matricula }, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
            window.location.pathname = window.location.pathname;
        }
	} catch (error) {
        console.log(error);
	}
}

async function borrar(event) {
    event.preventDefault();
    let matriculaCampo = datos();
    let matricula = matriculaCampo.matricula;

    if(!matricula) {
        alert("Agregar una matricula");
    }
    else {
        await axios.post('/borrar', { matricula }, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
        window.location.reload();
    }
}

async function modificar(event) {
    event.preventDefault();

    try {
        let { matricula, nombre, domicilio, sexo, especialidad } = datos();

        if(!matricula || !nombre || !domicilio || !sexo || !especialidad) {
            alert("Existen campos vacios");
        }

        else {
            await axios.post('/modificar', {
                matricula,
                nombre,
                domicilio,
                sexo,
                especialidad,
            }, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
            window.location.reload();
        }
    }
    catch (error) {
        console.log(error);
    }
}

btnAgregar.addEventListener("click", insertar);
btnBuscar.addEventListener("click", buscar);
btnBorrar.addEventListener("click", borrar);
btnModificar.addEventListener("click", modificar);